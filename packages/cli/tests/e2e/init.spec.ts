import chalk from "chalk";
import * as tc from "test-console";

import { main } from "../../src/main";

const DEFAULT_NODE_ARGS = ["", ""];

describe("running the app without a jira-shell", () => {
  it("should recommend the init command", async () => {

    const expected = chalk.yellow.bold([
      ">> You are using the jira-shell without a .jira-shell project file.",
      ">> Use `jira-shell init` to create a jira-shell project"
    ].join("\n"));

    const stdout = tc.stdout.inspect();

    await main([
      ...DEFAULT_NODE_ARGS,
      "--url",
      "http://test.jira",
      "--username",
      "testuser",
      "--password",
      "ls",
      "daily"
    ], {
        exitProcess: false
      });
    stdout.restore();

    expect(stdout.output.join().includes(expected)).toBeTruthy();
  });
});
