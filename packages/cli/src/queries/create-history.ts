import { addDays, min, startOfDay } from "date-fns";
import { endOfDay } from "date-fns";
import { chain, cloneDeep, identity, orderBy } from "lodash";

import { JiraIssue, JiraIssueFields, JiraSprint } from "@jira-shell/core/jira-client";

/**
 * Get the history of issues on a sprint from sprint start to now.
 * @returns a snapshot of the issues on the sprint from sprintStart to sprintEnd or today if
 * today is < endDate.
 *
 * **Important:** The first entry will be the representation of the issues at the sprintStart.
 * Then the iterator moves from endOfDay(sprintStart) to endOfDay(sprintEnd)!
 */
export function* createHistory(
  sprint: JiraSprint,
  issues: JiraIssue[]
): IterableIterator<{ date: Date; issues: JiraIssue[] }> {
  yield {
    date: new Date(sprint.startDate),
    issues: issues
      .filter(wasOnSprint(sprint, new Date(sprint.startDate)))
      .map(issue => getIssueHistorySnapshot(issue, new Date(sprint.startDate)))
  };

  const startDate = startOfDay(new Date(sprint.startDate));
  const endDate = min([new Date(), new Date(sprint.endDate)]);

  for (let date = startDate; date <= endDate; date = addDays(date, 1)) {
    const sprintIssues = issues
      .filter(wasOnSprint(sprint, endOfDay(date)))
      .map(issue => getIssueHistorySnapshot(issue, endOfDay(date)));
    yield { date, issues: sprintIssues };
  }
}

function wasOnSprint(sprint: JiraSprint, date: Date) {
  return (issue: JiraIssue) => {
    if (new Date(issue.fields.created) > date) {
      return false;
    }

    let sprints = (issue.fields.sprints || []).map(s => s.id);

    if (!issue.changelog) {
      return sprints.some(s => s === sprint.id);
    }

    const historiesUntil = orderBy(
      issue.changelog.histories.filter(h => new Date(h.created) >= date),
      h => h.created,
      "desc"
    );

    for (const history of historiesUntil) {
      const sprintChange = history.items.find(item => item.field === "Sprint");

      if (!sprintChange) {
        continue;
      }

      const fromSprints = sprintChange.from
        ? sprintChange.from.split(",").map(s => parseInt(s, 0))
        : [];
      const toSprints = sprintChange.to
        ? sprintChange.to.split(",").map(s => parseInt(s, 0))
        : [];

      sprints = sprints.filter(s => !toSprints.includes(s));
      sprints = [...sprints, ...fromSprints];
    }

    return sprints.includes(sprint.id);
  };
}

/**
 * Calculates an earlier version of the issue by applying
 * its history changes in a reversed order
 */
function getIssueHistorySnapshot(issue: JiraIssue, date: Date) {
  if (!issue.changelog) {
    return issue;
  }

  const issueSnapshot = cloneDeep(issue);

  const historyAfterSnapshotDate = chain(issue.changelog.histories)
    .orderBy(h => h.created, "desc")
    .takeWhile(h => new Date(h.created) >= date)
    .valueOf();

  const parseIntWithoutNull = (val: string) => (!val ? 0 : parseInt(val, 0));

  // defines which fields are updated and how
  const fieldDefinitions: Partial<
    Record<keyof JiraIssueFields, (str: string) => any>
  > = {
    timeestimate: parseIntWithoutNull,
    timeoriginalestimate: parseIntWithoutNull,
    aggregatetimespent: parseIntWithoutNull,
    assignee: identity,
    created: identity,
    priority: identity,
    rank: identity,
    resolutiondate: identity,
    status: identity,
    summary: identity,
    timespent: parseIntWithoutNull,
    updated: identity
  };

  for (const history of historyAfterSnapshotDate) {
    for (const { from, field } of history.items) {
      const issueField = field as keyof JiraIssueFields;
      const parser = fieldDefinitions[issueField];
      if (!parser) {
        continue;
      }
      issueSnapshot.fields[issueField] = parser(from);
    }
  }

  return issueSnapshot;
}
