import { JiraIssue, JiraSprint } from "@jira-shell/core/jira-client";
import { createHistory } from "../../queries/create-history";

export function* calculateSprintScopeReport(sprint: JiraSprint, issues: JiraIssue[]): IterableIterator<ScopeReportRow> {

  const history = [...createHistory(sprint, issues)];
  const original = history[0];
  let prev = original;
  let sumAdded = 0;

  for (const current of history.slice(1)) {
    // current issues that were not on the prev list
    const added = current.issues.filter(({ key }) => !prev.issues.some(issue => issue.key === key)).length;

    sumAdded += added;

    yield {
      date: current.date,
      added,
      // prev issues that are not in the current list
      removed: prev.issues.filter(({ key }) => !current.issues.some(issue => issue.key === key)).length,
      // original issues that are still on the current list
      sumOriginal: original.issues.filter(({ key }) => prev.issues.some(issue => key === issue.key)).length,
      sumAdded,
      sumTotal: current.issues.length
    };

    prev = current;
  }
}

/**
 * The data structure of the scope report.
 * Contains the aggregated movements of issues from or to a sprint
 */
export interface ScopeReportRow {

  /**
   * Date when the scope of the sprint changed
   */
  date: Date;

  /**
   * Number of issues added on this date
   */
  added: number;

  /**
   * Number of issues removed on this date
   */
  removed: number;

  /**
   * Total number of remaining original issues, that have been added initially
   * and not after the sprint start
   */
  sumOriginal: number;

  /**
   * Total number of ticket added after the beginning of the sprint
   */
  sumAdded: number;

  /**
   * Total number of tickets on the sprint
   */
  sumTotal: number;
}
