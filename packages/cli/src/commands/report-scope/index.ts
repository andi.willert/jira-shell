/**
 * The report:scope command prints all changes of the sprint scope by day.
 *
 * Changes are:
 *  - tickets added after the sprint start
 *  - tickets removed after the sprint start
 */

import { format } from "date-fns";

import { formatDataTable, TableFormat } from "@jira-shell/core/formatting";
import { getAllSprints, getSprint, JiraIssue } from "@jira-shell/core/jira-client";
import { loadIssuesRelatedToSprint } from "../../queries/load-issues-related-to-sprint";
import { WithContextCommandModule } from "../../shared/with-context";
import { calculateSprintScopeReport, ScopeReportRow } from "./report";

interface ReportScopeArgs {
  sprint?: string;
  format: TableFormat | "json";
}

export const reportScopeModule: WithContextCommandModule = {
  builder: y =>
    y
      .option("sprint", {
        description: "a sprint name or a variable: __current or __next"
      })
      .option("format", {
        description: "defines the output format: ascii, csv, json",
        choices: ["json", "csv", "ascii"],
        default: "ascii"
      }),
  command: "report:scope",
  async *handler(request: { args: ReportScopeArgs }) {
    // find the sprint for which we want to create the report
    const sprints = yield () => getAllSprints();
    const sprint = getSprint(request.args.sprint || "__current", sprints);

    if (!sprint) {
      yield `sprint ${request.args.sprint} not found\n`;
      return;
    }

    yield `retrieving history for the scope report of sprint '${
      sprint ? sprint.name : "?"
    }'...\n`;

    // load all relevant issues: issues that are on the sprint
    // or issues that have been removed from it
    const issues: JiraIssue[] = yield* loadIssuesRelatedToSprint(sprint);

    yield `calculating report data...\n`;

    // create report data by crunching through the history
    // of each ticket to determine their movements in the course
    // of the sprint
    const report = calculateSprintScopeReport(sprint, issues);

    yield print([...report], request.args.format);
  }
};

function* print(report: ScopeReportRow[], tableFormat: TableFormat | "json") {
  if (tableFormat === "json") {
    return JSON.stringify([...report]);
  }

  return formatDataTable<ScopeReportRow>({
    data: report,
    columns: [
      [
        "date",
        {
          label: "Date",
          valueFormatter: (value: Date) =>
            tableFormat === "ascii"
              ? format(value, "dd.MM.yyyy")
              : value.toISOString()
        }
      ],
      ["added", "Added"],
      ["removed", "Removed"],
      ["sumOriginal", "∑ Original"],
      ["sumAdded", "∑ Added"],
      ["sumTotal", "∑ Total"]
    ],
    format: tableFormat
  });
}
