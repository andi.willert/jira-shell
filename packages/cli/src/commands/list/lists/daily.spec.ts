import * as dateFns from "date-fns";
import { flatten } from "lodash";
import stripAnsi from "strip-ansi";

import { JiraIssue, SearchResponse } from "@jira-shell/core/jira-client";
import { executeIterator } from "@jira-shell/core/modules";
import { ApplicationContext } from "../../../shared/with-context";
import { listIssuesForDailyMeeting } from "./daily";

const emptyResponse = {
  pageSize: 50,
  total: 0,
  iterator: [],
  toPromise: () => Promise.resolve([])
} as any as SearchResponse;

// TODO: @pbedat spec
describe("ls daily", () => {

  it("should contain three paragraphs: Resolved, In Progress, Open", async () => {

    const iterator = listIssuesForDailyMeeting({
      url: "http://jira.test"
    } as ApplicationContext);

    const buffer = await executeAndRecord(iterator, emptyResponse);

    const paragraphs = buffer.split("\n\n");

    const headlines = paragraphs.map(p => p.split("\n")[0]);

    expect(headlines).toEqual(["Resolved", "In Progress", "Open (next ~8h)"]);
  });

  it("should display warnings if there is a status without issues", async () => {
    const iterator = listIssuesForDailyMeeting({
      url: "http://jira.test"
    } as ApplicationContext);

    const buffer = await executeAndRecord(iterator, emptyResponse);

    const paragraphs = buffer.split("\n\n");
    const status = ["Resolved", "In Progress", "Open"];

    paragraphs.map((p, i) => expect(p).toContain(`>> no issues '${status[i]}`));
  });

  describe("Resolved issues", () => {
    const today = dateFns.startOfDay(new Date());

    const issues = [
      ["TEST1", "Resolved", 2 * 3600, dateFns.addHours(today, 8)],
      ["TEST2", "Resolved", 3 * 3600, dateFns.addHours(today, 7)],
      ["TEST3", "Resolved", 4 * 3600, dateFns.addHours(today, 9)]
    ];

    const jiraResponse: SearchResponse = {
      pageSize: 50,
      total: 3,
      iterator: issues.map(tuple => toJiraIssue(tuple as any)) as any as AsyncIterableIterator<any>,
      toPromise: () => Promise.resolve(issues.map(tuple => toJiraIssue(tuple as any)))
    };

    const iterator = listIssuesForDailyMeeting({
      url: "http://jira.test"
    } as ApplicationContext);

    it("should order by resolution date", async () => {
      const buffer = await executeAndRecord(iterator, jiraResponse);

      const paragraphs = buffer.split("\n\n");

      const openIssues = paragraphs[0].split("\n").slice(2);
      const ticketIds = openIssues.map(issue => issue.split(/\s/)[0]);
      expect(ticketIds).toEqual(["TEST2", "TEST1", "TEST3"]);

    });
  });

  describe("In Progress issues", () => {

    const issues = [
      ["TEST1", "In Progress", 2 * 3600, , "2"],
      ["TEST2", "In Progress", 3 * 3600, , "3"],
      ["TEST3", "In Progress", 4 * 3600, , "1"]
    ];

    const jiraResponse: SearchResponse = {
      total: 3,
      pageSize: 50,
      iterator: issues.map(tuple => toJiraIssue(tuple as any)) as any,
      toPromise: () => Promise.resolve(issues.map(tuple => toJiraIssue(tuple as any)))
    };

    const iterator = listIssuesForDailyMeeting({
      url: "http://jira.test"
    } as ApplicationContext);

    it("should order by rank", async () => {
      const buffer = await executeAndRecord(iterator, jiraResponse);
      const paragraphs = buffer.split("\n\n");
      const inProgress = paragraphs[1].split("\n").slice(2);

      expect(inProgress.map(issue => issue.split(/\s/)[0])).toEqual(["TEST3", "TEST1", "TEST2"]);
    });
  });

  describe("Open issues", () => {

    const issues = [
      ["TEST4", "Open", 8 * 3600, , "4"],
      ["TEST1", "Open", 2 * 3600, , "2"],
      ["TEST2", "Open", 3 * 3600, , "3"],
      ["TEST3", "Open", 4 * 3600, , "1"]
    ];

    const jiraResponse: SearchResponse = {
      total: 3,
      pageSize: 50,
      iterator: issues.map(tuple => toJiraIssue(tuple as any)) as any,
      toPromise: () => Promise.resolve(issues.map(tuple => toJiraIssue(tuple as any)))
    };

    it("should order by rank", async () => {
      const iterator = listIssuesForDailyMeeting({
        url: "http://jira.test"
      } as ApplicationContext);
      const buffer = await executeAndRecord(iterator, jiraResponse);
      const paragraphs = buffer.split("\n\n");

      const inProgress = paragraphs[2].split("\n").slice(2, 5);

      expect(
        inProgress.map(issue => issue.split(/\s/)[0])
      ).toEqual(["TEST3", "TEST1", "TEST2"]);
    });

    it("should limit to 8h", async () => {
      const iterator = listIssuesForDailyMeeting({
        url: "http://jira.test"
      } as ApplicationContext);
      const buffer = await executeAndRecord(iterator, jiraResponse);
      const paragraphs = buffer.split("\n\n");
      const overflowHint = paragraphs[2].split("\n").slice(5)[0];

      expect(overflowHint).toBe(">> 1 issues to go: 8 h");
    });
  });
});

// tslint:disable-next-line:max-line-length
async function executeAndRecord(iterator: AsyncIterableIterator<any> | IterableIterator<any>, jiraResponse: SearchResponse): Promise<string> {
  let buffer = "";

  await executeIterator(
    iterator, {
      interceptHandler: (effect, defaultHandler) => {
        // gather all writes in a buffer, stripped from colors
        if (effect.type === "WRITE") {
          buffer += flatten([effect.payload.buffer]).map(stripAnsi).join("");
          return undefined;
        }

        // replace the jiraClient call with the fake jiraResponse
        if (effect.type === "CALL") {
          return jiraResponse;
        }

        // handle all other effects, with the default handler
        return defaultHandler(effect);
      }
    });

  return buffer;
}

function toJiraIssue(tuple: [string, string, number, string, string]) {
  const [key, status, timeestimate, resolutiondate, rank] = tuple;

  return {
    key,
    fields: {
      summary: key,
      resolutiondate,
      status: {
        name: status
      },
      timeestimate,
      rank
    }
  } as JiraIssue;
}
