import chalk from "chalk";
import { compact } from "lodash";

import * as jira from "@jira-shell/core/jira-client";

/**
 * lists all sprints of all projects
 * @param args
 */
export async function listSprints() {
  const sprints = await jira.getAllSprints();

  // will print output like
  // Sprint1 open
  // Sprint2 closed
  compact(sprints).forEach((sprint) => {
    process.stdout.write(`${chalk.bold(sprint.name)} ${sprint.state}\n`);
  });
}
