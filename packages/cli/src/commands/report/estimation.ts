import chalk from "chalk";
import { sum } from "lodash";
import { Writable } from "stream";
import { table } from "table";

import { formatHours } from "@jira-shell/core/formatting";
import { JiraIssue, search } from "@jira-shell/core/jira-client";

export async function createEstimationReport(args: { issues: string[], project: string }) {

  // when the stdin is a TTY
  const issues = process.stdin.isTTY
    // get all issues from the project with the issues provided in the args
    ? (await search({ jql: `project=${args.project} AND id IN (${args.issues.join(",")}) ORDER BY key` }))
    // or from the stdin if stdin is no TTY
    : await getIssuesFromStdin();

  /**
   * Render a table with the following data:
   *
   * | Issue | Original Estimate | Actual Time Spent | Summary   |
   * +-------+-------------------+-------------------+-----------+
   * | <key> | <estimate> h      | <time spent> h    | <summary> |
   * +-------+-------------------+-------------------+-----------+
   * | Total | <estimate> h      | <time spent> h    |           |
   */

  const dataTable = createDataTable(
    issues,
    [
      ["Issue", ({ key }) => key],
      ["Original Estimate", ({ fields }) => fields.timeoriginalestimate],
      ["Actual Time Spent", ({ fields }) => fields.aggregatetimespent],
      ["Summary", ({ fields }) => fields.summary]
    ]
  );

  // sum up the footer values
  const totalOriginalEstimate = sum(issues.map(({ fields }) => fields.timeoriginalestimate));
  const totalTimeSpent = sum(issues.map(({ fields }) => fields.aggregatetimespent));

  // format the footer values
  const footer = [
    "Total",
    formatHours(totalOriginalEstimate),
    formatTimeSpent(totalOriginalEstimate, totalTimeSpent),
    ""
  ].map((str) => chalk.bold(str));

  // render the table
  process.stdout.write(table([
    dataTable.columns,
    ...dataTable.rows.map((row) => [
      row[0],
      formatHours(row[1]),
      formatTimeSpent(row[1], row[2]),
      `${(row[3] || "").substr(0, 50)}${(row[3] && row[3].length > 50) ? "..." : ""}`
    ]),
    footer
  ], {
      columns: [{ alignment: "right" }, { alignment: "right" }, { alignment: "right" }]
    }));
}

/**
 * Format the time spent based on the original estimate as hours
 * It uses colors to show the deviation from the original estimate
 *  - less than 10% of the original estimate: green
 *  - more than 10% of the original estimate: yellow
 *  - more than 20% of the original estimate: red
 *
 * Example:
 * formatTimeSpent(3600, 7200) => red(2 h)
 * @returns the colorized time spent as hours
 * @param originalEstimate
 * @param timeSpent
 */
function formatTimeSpent(originalEstimate: number, timeSpent: number) {
  const deviation = timeSpent - originalEstimate;

  if (deviation >= (originalEstimate * 0.2)) {
    return chalk.red(formatHours(timeSpent));
  }
  if (deviation > (originalEstimate * 0.1)) {
    return chalk.yellow(formatHours(timeSpent));
  }

  return chalk.green(formatHours(timeSpent));
}

function createDataTable<T, TKey extends string>(
  data: T[],
  definitions: Array<[TKey, (entry: T) => any]>
) {
  const rows = data.map((d) => definitions.map((definition: any) => definition[1](d)));
  return {
    columns: definitions.map((definition) => definition[0]),
    rows
  };
}

/**
 * Reads JiraIssues from the stdin
 * expects the stdin stream to be a JiraIssue array serialized as json
 */
function getIssuesFromStdin(): Promise<JiraIssue[]> {
  if (process.stdin.isTTY) {
    process.stderr.write(`cannot get issues from stdin: stdin is a tty\n`);
    return Promise.resolve([]);
  }

  return new Promise((resolve) => {
    let json = "";
    const stream = process.stdin.pipe(new Writable({
      write: (chunk, encoding, callback) => {
        json += chunk;
        callback();
      }
    }));
    stream.on("drain", () => {
      resolve(JSON.parse(json));
    });
  });
}
