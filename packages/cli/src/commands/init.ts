import chalk from "chalk";
import * as fs from "fs";
import { fromPairs } from "lodash";
import * as path from "path";
import * as readline from "readline";
import * as URL from "url";
import * as yargs from "yargs";

import { configure, getFields } from "@jira-shell/core/jira-client";
import { JiraShellProject } from "@jira-shell/core/project";
import { demandMissingCredentials } from "../shared/with-context";

interface Options {
  url?: string;
  username?: string;
  project?: string;
}

/**
 * initializes a jira-shell project file
 * all options are optional and are queried from stdin if not provided
 */
export const initModule: yargs.CommandModule<any, any> = {
  describe: "initializes a jira-shell project",
  command: "init",
  builder: (_: any) => _
    .option("url", { description: "URL to a JIRA instance" })
    .option("username", { alias: "u" })
    .option("project", { alias: "p" }),
  handler: async (options: Options) => {

    const rl = readline.createInterface(process.stdin, process.stdout, undefined, true);

    const url =
      options.url
      || await query(rl, "jira base url:", {
        "invalid url": (u: string) => (URL.parse(u).protocol || "").startsWith("http")
      })
      || "";

    const username = options.username || await query(rl, "jira username (optional):");
    const project = options.project || await query(rl, "jira project key:", { required: (p: string) => !!p }) || "";

    const jiraShellProject: JiraShellProject = {
      url,
      username,
      project
    };

    // remove undefined values from project
    Object
      .keys(jiraShellProject)
      .forEach((key) => ((jiraShellProject as any)[key] === undefined) && delete (jiraShellProject as any)[key]);

    const question = [
      "",
      JSON.stringify(jiraShellProject, null, 2),
      "",
      `do you like what you see? [Y/n]`,
      ""
    ].join("\n");

    const yesNo = await new Promise(
      (resolve) => rl.question(question, (yn) => resolve((yn || "y").toUpperCase())));

    rl.close();

    if (yesNo !== "Y") {
      return;
    }

    const projectPath = process.cwd();
    const projectFilePath = path.resolve(projectPath, ".jira-shell");

    fs.writeFileSync(projectFilePath, JSON.stringify(jiraShellProject, undefined, 2));

    process.stdout.write(chalk.green("✓ project file created\n\n"));

    process.stdout.write("fetching custom fields from jira...\n");
    await readCustomFields(jiraShellProject, projectFilePath);
    process.stdout.write(chalk.green("✓ custom fields saved\n"));
  }
};

async function query(
  rl: readline.ReadLine,
  question: string,
  validators: { [validationKey: string]: (str: string) => boolean } = {}): Promise<string | undefined> {
  let input: string | undefined = "";

  for (; ;) {
    input = await new Promise<string | undefined>((resolve) => rl.question(`${question} `, resolve));

    let valid = true;
    for (const validator in validators) {
      if (!validators[validator]) {
        continue;
      }
      valid = validators[validator](input || "");
      if (!valid) {
        console.log(validator);
        // rl.write(validator);
        break;
      }
    }

    if (valid) {
      return input === "" ? undefined : input;
    }
  }
}

/**
 * Reads the custom fields from the newly configured jira instance
 * and writes them to the .jira-shell project file
 * @param jiraShellProject
 * @param projectFilePath
 */
async function readCustomFields(jiraShellProject: JiraShellProject, projectFilePath: string) {
  const { username, project, url } = jiraShellProject;

  // we need to call jira for the fields, so credentials are necessary
  const credentials = await demandMissingCredentials({ username });

  // configure the jira client
  configure({
    credentials,
    project,
    url,
    customFields: jiraShellProject.customFields || {}
  });

  const fields = await getFields();
  const customFields = fields.filter(f => f.custom);

  // create a dictionary mapping field names to their id
  jiraShellProject.customFields = fromPairs(customFields.map(c => [c.name.toLowerCase(), c.id]));

  // write the updated .jira-shell project file
  fs.writeFileSync(projectFilePath, JSON.stringify(jiraShellProject, undefined, 2));
}
