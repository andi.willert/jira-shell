import { JiraAssignee } from "@jira-shell/core/jira-client";

export const ENV_VARS = {
  username: "JIRA_SHELL_USERNAME",
  password: "JIRA_SHELL_PASSWORD_BASE64"
};

// virtual JiraAssignee for tickets without assignee
export const UNASSIGNED: JiraAssignee = { name: "", displayName: "Unassigned" };
