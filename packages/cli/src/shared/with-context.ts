import chalk from "chalk";
import { enableHyperlinks } from "cli-hyperlinks";
import { createInterface } from "readline";
import * as yargs from "yargs";

import * as jiraClient from "@jira-shell/core/jira-client";
import { executeIterator, isAsyncIterable, isIterable } from "@jira-shell/core/modules";
import { MutableStdOut } from "@jira-shell/core/mutable-stdout";
import { getJiraShellProject, UNDEFINED_PROJECT } from "@jira-shell/core/project";
import { ENV_VARS } from "../constants";

/**
 * Higher order module
 * Decorates the `handler` of a `CommandModule` to provide
 * an `ApplicationContext` to the decorated module by
 * - querying for the password if required
 * - reading the .jira-shell project file
 * @param commandModule
 */
export function withContext(commandModule: WithContextCommandModule): yargs.CommandModule<any, any> {

  const jiraShellProject = getJiraShellProject();

  return {
    ...commandModule,
    builder: ((y: yargs.Argv) => {
      return commandModule.builder(
        // require the specification of a url, when no jira-shell project was found
        y.option("url", {
          required: jiraShellProject === UNDEFINED_PROJECT,
          desc: "base url of the Jira instance"
        })
      );
    }),
    async handler(args: yargs.Arguments<any>) {

      if (args.hyperlinks) {
        await enableHyperlinks();
      }

      const username = args.username || jiraShellProject.username;
      const password = args.password;

      const credentials = await demandMissingCredentials({ username, password });
      const { url, project, customFields } = jiraShellProject;

      const applicationContext = {
        credentials,
        url: url || args.url,
        project,
        customFields: customFields || {}
      };

      jiraClient.configure(applicationContext);

      const result = commandModule.handler({ args, context: applicationContext });

      if (isAsyncIterable(result)) {
        return await executeIterator(result);
      }

      if (!isIterable(result)) {
        return await result;
      }

      await executeIterator(result);
    }
  };
}

export interface WithContextCommandModule extends yargs.CommandModule<any, any> {
  builder(argv: yargs.Argv): yargs.Argv;
  handler(request: { args: any, context: ApplicationContext }): any;
}

export interface ApplicationContext {
  credentials: {
    username: string,
    password: string,
  };
  url: string;
  project: string;
}

/**
 * queries the username or password from the stdin if they are not provided in the credentials
 * @param credentials
 */
export async function demandMissingCredentials(credentials: { username?: string, password?: string }) {
  const mutableStdout = new MutableStdOut();

  const rl = createInterface(process.stdin, mutableStdout.stream, undefined, true);

  const username =
    credentials.username
    || process.env[ENV_VARS.username]
    || await new Promise<string>((resolve) => rl.question("user: ", resolve));

  // get the password from the provided args
  const password = credentials.password
    // ...or from the env variable (base64 encoded)
    || (process.env[ENV_VARS.password] && Buffer.from(process.env[ENV_VARS.password] || "", "base64").toString())
    // ...or why not ask the user for his password?
    || await new Promise<string>((resolve) => {
      rl.question("password: ", (buffer) => {
        mutableStdout.unmute();
        resolve(buffer);
        process.stdout.write("\n");
      });
      mutableStdout.mute();
    });

  rl.close();
  process.stdout.write("\n");

  return { username, password };
}
