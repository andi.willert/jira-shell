// the following line can be used to produce --help outputs
// referencing "jira-shell" instead of "src/" or "src/index" as entry point
// process.argv[1] = "jira-shell";

import chalk from "chalk";
import yargs = require("yargs");

import { checkEngineCompatibility } from "@jira-shell/core/compat";
import {
  getJiraShellProject,
  UNDEFINED_PROJECT
} from "@jira-shell/core/project";
import { initModule } from "./commands/init";
import { listModule } from "./commands/list";
import { reportModule } from "./commands/report";
import { reportScopeModule } from "./commands/report-scope";
import { reportActualTargetModule } from "./commands/report-target-actual";
import { timesheetModule } from "./commands/timesheet";
import { workloadModule } from "./commands/workload";
import { ENV_VARS } from "./constants";
import { withContext } from "./shared/with-context";

interface Options {
  exitProcess: boolean;
}

const DEFAULT_OPTIONS: Options = {
  exitProcess: true
};

export async function main(argv?: string[], options = DEFAULT_OPTIONS) {

  checkEngineCompatibility();

  const jiraShellProject = getJiraShellProject();

  if (jiraShellProject === UNDEFINED_PROJECT) {
    process.stdout.write(
      chalk.yellow.bold(
        [
          ">> You are using the jira-shell without a .jira-shell project file.",
          ">> Use `jira-shell init` to create a jira-shell project\n\n"
        ].join("\n")
      )
    );
  }

  let arg = yargs
    .option("username", {
      alias: "u",
      desc: `Can be preset with env var ${ENV_VARS.username}`
    })
    .option("password", {
      alias: "p",
      desc: `Can be preset with env var ${ENV_VARS.password} in base64 encoding`
    })
    .option("hyperlinks", {
      alias: "l",
      type: "boolean",
      default: false,
      desc: "experimental cli-hyperlinks integration"
    })
    .command(withContext(listModule))
    .command(withContext(workloadModule))
    .command(withContext(reportModule))
    .command(withContext(reportScopeModule))
    .command(withContext(reportActualTargetModule))
    .command(withContext(timesheetModule))
    .command(initModule)
    .exitProcess(options.exitProcess)
    .help();

  if (process.stdin.isTTY) {
    arg = arg.demandCommand();
  }
  try {
    return arg.parse((argv || process.argv).slice(2));
  } catch (err) {
    console.error(err);
  }
}
