Authoring Command Modules
=========================

The architecture of this app is built on the command modules of yargs.
Those modules are enhanced by the `withContext` wrapper, which provides
the common behaviour of the application and the framework, that is used
to create console commands.

## wihContext

Modules are added to the application in the `src/main.ts` file:

```typescript
yargs
  .command(withContext(listModule))
```

The `withContext` *higher order module* enhances the capabilities of the normal yargs command modules,
acting as a middleware wrapper around the yargs command modules.

What `withContext` is actually doing:

- loads the application context from the `.jira-shell` project file
- handles the absence of the `.jira-shell` project file
- reads credentials from ENV or stdin on demand
- enables the experimental `cli-hyperlinks` integration
- executes command iterators

## Creating a module

Modules are plain object of the type `WithContextCommandModule`:

```typescript
export const echo: WithContextCommandModule = {
describe: "prints text to the stdout",
  command: "echo",
  builder: (y: yargs.Argv) =>
    y.positional("text"),
  handler({args, context}) {
    process.stdout.write(args.text");
  }
}
```

Most of the properties are derived from the `yargs.CommandModule`, like `command`, `builder`, `describe`, ...

The `handler` which originally takes only an args object, with the parsed arguments,
can also receive an `ApplicationContext`:

```typescript
interface ApplicationContext {
  credentials: {
    username: string,
    password: string,
  };
  url: string;
  project: string;
}
```

Most of those values are provided by the `.jira-shell` file, the ENV or read from the stdin.

## Writing handlers

You can write a handler in three ways:
 - a plain and simple function
 - an async function
 - a function returning an IterableIterator<ConsoleEffects>

When the handler returns a Promise, which might be the case in the first to implementation, it waits for the promise to be resolved.

All three implementations can be used, to write very sophisticated commands,
but the third implementation results in easier testing and less boilerplate

Example:

```typescript
function* handler({args, context}) {
  // functions are evaluated - if they return a promise, the resolved
  // values will be provided back to the iterator
  const issues: JiraIssue[] = yield () => jiraClient.search(args.jql);

  if(issues.length === 0) {
    // yielding string will print them to the stdout
    return "no issues found!\n";
  }

  for(const issue of issues) {
    yield issue.id;
    // yielding another iterator will execute the iterator
    yield getWorklog(issue);
  }
}

function* getWorklog(issue: JiraIssue) {
  const {assignee, timespent} = yield () => jiraClient.getWorklog(issue);
  // string[] will be concatenated and printed to the stdout
  return worklog.map(w => `${assignee}: ${formatHours(timespent)}\n`);
}
```