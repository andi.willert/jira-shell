import * as cp from "child_process";
import * as fs from "fs";
import * as path from "path";
import { EndOfLineState } from "typescript";

render();

async function render() {

  const readmeBuffer = fs.readFileSync(path.resolve(__dirname, "../../README.src.md"));

  const lines = readmeBuffer.toString().split(/\r?\n/);

  const SUBSTITUTION_PATTERN = /^\$\(.+?\)$/;

  const renderResult = lines.map(line => {
    if (!SUBSTITUTION_PATTERN.test(line)) {
      return Promise.resolve(line);
    }

    const command = line.substring(2, line.length - 1);

    console.log("found command", command);

    return new Promise((resolve) => cp.exec(command, (err, stdout) => {
      if (err) {
        console.error(`ERROR: unable to render '${line}': ${err.message}`);
        console.log(err.stack);
        return resolve(`<ERROR> ${err.message}`);
      }

      resolve([
        "```",
        stdout,
        "```"
      ].join("\n"));
    }));
  });

  const renderedLines = await Promise.all(renderResult);

  fs.writeFileSync(path.resolve(__dirname, "../../README.md"), renderedLines.join("\n"));
}
