export async function* burst<T>(size: number, iterator: IterableIterator<T>) {

  console.log("take buff", size);
  let buffer = await take(size, iterator);

  while (buffer.length !== 0) {
    console.log("yield buff", size);
    yield* buffer;
    console.log("take buff", size);
    buffer = await take(size, iterator);
  }

  console.log("end take", size);
}

export async function take<T>(count: number, iterator: IterableIterator<T>) {
  const buffer = [];

  for (let i = 0; i < count; i++) {
    const result = iterator.next();
    buffer.push(result);
  }

  console.log("resolve take buffer", buffer);

  const results = await Promise.all(buffer);
  console.log("take buffer resolved", count);
  const takeResults = [];
  for (const result of results) {
    if (result.done) {
      break;
    }

    takeResults.push(result.value);
  }

  return takeResults;
}

export async function reduce<T, U>(iterator: IterableIterator<T>, reducer: (prev: U, item: T) => U, initial: U) {
  let current = initial;
  for await (const item of iterator) {
    current = reducer(current, item);
  }
  return current;
}
