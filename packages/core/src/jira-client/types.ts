
export interface JiraResponse {
  expand: string;
  startAt: number;
  maxResults: number;
  total: number;
}

export interface JiraSearchResponse {
  issues: JiraIssue[];
}
export interface JiraAssignee {
  name: string;
  displayName: string;
}
export interface JiraIssueStatus {
  name: string;
  description: string;
  iconUrl: string;
  id: string;
  statusCategory: {
    self: string,
    id: number,
    key: "new" | "done" | "indeterminate",
    colorName: string,
    name: string
  };
}
interface JiraIssuePriority {
  id: string;
  name: string;
}

export interface JiraIssueFields {
  timeestimate: number;
  updated: string;
  summary: string;
  assignee: JiraAssignee;
  resolutiondate: string;
  status: JiraIssueStatus;
  priority: JiraIssuePriority;
  timeoriginalestimate: number;
  timespent: number;
  aggregatetimespent: number;
}

export interface JiraIssue {
  expand: string;
  id: number;
  self: string;
  key: string;
  changelog?: JiraChangelog;
  fields: JiraIssueFields
  // custom fields
  & { [key: string]: string | number | undefined };
}

export interface JiraIssueFields extends ParsedCustomFields {
  timeoriginalestimate: number;
  timespent: number;
  aggregatetimespent: number;
  timeestimate: number;
  updated: string;
  summary: string;
  assignee: JiraAssignee;
  resolutiondate: string;
  status: JiraIssueStatus;
  priority: JiraIssuePriority;
  created: string;
}

export interface JiraChangelog extends JiraResponse {
  histories: JiraHistoryEntry[];
}

export interface JiraHistoryEntry {
  id: string;
  created: string;
  items: JiraHistoryEntryItem[];
}

export interface JiraHistoryEntryItem {
  field: keyof JiraIssue | string;
  fieldtype: string;
  from: string;
  fromString: string;
  to: string;
  toString: string;
}

/**
 * fields that are not included in the jira rest api response but parsed by
 * the jira client
 */
interface ParsedCustomFields {
  rank: string;
  sprints?: Array<{
    rapidViewId: string
    state: string
    startDate: string
    name: string
    endDate: string
    completeDate: string
    sequence: string
    id: number
  }>;
}

export interface JiraSprintsResponse {
  values: JiraSprint[];
}

export interface JiraSprint {
  id: number;
  self: string;
  state: string | "closed";
  name: string;
  startDate: string;
  endDate: string;
  completeDate: string;
  originBoardId: number;
}

export interface JiraField {
  id: string;
  name: string;
  custom: boolean;
  orderable: boolean;
  navigable: boolean;
  searchable: boolean;
  clauseNames: string[];
  schema: {
    type: string,
    system: string
  };
}

interface JiraAuthor {
  self: string;
  name: string;
  displayName: string;
  active: boolean;
}

export interface JiraWorklog {
  self: string;
  author: JiraAuthor;
  updateAuthor: JiraAuthor;
  comment: string | {
    type: string,
    version: number,
    content: Array<{
      type: string,
      content: Array<{
        type: string,
        text: string
      }>
    }>
  };
  updated: string;

  started: string;
  timeSpent: string;
  timeSpentSeconds: number;
  id: number;
  issueId: number;
}

export type JiraWorklogResponse = JiraResponse & { worklogs: JiraWorklog[] };
