import * as http from "http";
import * as https from "https";
import { compact, flatten, fromPairs, range } from "lodash";
import * as Url from "url";

import { iteratorToArray } from "../utils/iterator";
import {
  JiraField, JiraIssue, JiraIssueStatus, JiraResponse, JiraSearchResponse, JiraSprintsResponse, JiraWorklogResponse
} from "./types";

interface JiraClientContext {
  credentials: {
    username: string,
    password: string,
  };
  project: string;
  url: string;
  customFields: Record<string, string>;
}

let context: JiraClientContext;

export function configure(jiraClientContext: JiraClientContext) {
  context = { ...jiraClientContext, customFields: jiraClientContext.customFields || {} };
}

interface SearchOptions {
  pageSize?: number;
  expand: Array<"changelog">;
}

export interface SearchResponse {
  pageSize: number;
  total: number;
  iterator: AsyncIterableIterator<JiraIssue>;
  toPromise: () => Promise<JiraIssue[]>;
}

const DEFAULT_OPTIONS: SearchOptions = {
  pageSize: 50,
  expand: []
};

/**
 * search for jira issues using JQL
 * @param request
 */
export async function searchAsync(request: { jql: string }, options: SearchOptions = DEFAULT_OPTIONS): Promise<SearchResponse> {

  const { jql } = request;
  const pageSize: number = options.pageSize || DEFAULT_OPTIONS.pageSize as number;

  let response = await execute<JiraSearchResponse>(
    // tslint:disable-next-line:max-line-length
    `/rest/api/2/search?maxResults=${pageSize || DEFAULT_OPTIONS.pageSize}&jql=${encodeURIComponent(jql)}&expand=${(options.expand || []).join(",")}`);

  async function* iterate(jiraResponse: JiraSearchResponse & JiraResponse): AsyncIterableIterator<JiraIssue> {

    for (const issue of jiraResponse.issues) {
      const { fields, changelog } = issue;
      // parse rank custom field
      fields.rank = fields[context.customFields.rang || context.customFields.rank] as string;

      // parse sprint custom field
      if (fields[(context.customFields || {} as any).sprint]) {
        fields.sprints = (fields[(context.customFields || {} as any).sprint] as any).map(parseSprintField);
      }

      if (changelog && changelog.total > changelog.maxResults) {
        console.log(`skipped changelog: total '${changelog.total}' loaded '${changelog.maxResults}'`);
      }

      yield issue;
    }

    if (jiraResponse.startAt + pageSize < response.total) {
      response = await execute<JiraSearchResponse>(
        // tslint:disable-next-line:max-line-length
        `/rest/api/2/search?maxResults=${pageSize}&startAt=${jiraResponse.startAt + pageSize}&jql=${encodeURIComponent(jql)}&expand=${(options.expand || []).join(",")}`);

      yield* iterate(response);
    }
  }

  return {
    pageSize,
    total: response.total,
    iterator: iterate(response),
    toPromise: () => iteratorToArray(iterate(response))
  };
}

export async function search(request: { jql: string }, options: SearchOptions = { expand: [] }) {

  const response = await searchAsync(request, options);

  return await response.toPromise();
}

export async function getWorklog({ key }: JiraIssue) {
  const PAGE_SIZE = 50;

  const worklog = await execute<JiraWorklogResponse>(`/rest/api/2/issue/${key}/worklog?maxResults=${PAGE_SIZE}`);

  const responses = await Promise.all(range(worklog.worklogs.length, worklog.total, PAGE_SIZE).map(startAt =>
    execute<JiraWorklogResponse>(`/rest/api/2/issue/${key}/worklog?startAt=${startAt}&maxResults=${PAGE_SIZE}`)
  ));

  return flatten([worklog, ...responses].map(({ worklogs }) => worklogs));
}

function parseSprintField(field: string): any {
  const matches = field.match(/\[([^\]]+)]/);

  if (matches === null) {
    return null;
  }

  const sprint = fromPairs(matches[1].split(",").map(token => token.split("=")));

  sprint.id = parseInt(sprint.sequence, 0);

  return sprint;
}

export async function getAllBoards() {
  return execute<JiraBoardsResponse>(`/rest/agile/1.0/board?projectKeyOrId=${context.project}`);
}

export function getFields() {
  return execute<JiraField[]>("/rest/api/2/field");
}

interface JiraBoardsResponse {
  values: Array<{
    id: number,
    self: string,
    name: string
    type: "scrum" | "kanban",
  }>;
}

export async function getAllSprints() {
  const { values } = await getAllBoards();

  const boardSprints = await Promise.all(values.map(async (b) => {
    try {
      return await getSprints({ boardId: b.id });
    } catch (err) {
      // TODO add logging here:
      // commented this out because of the --quiet option
      // process.stderr.write(`unable to fetch sprints for board ${b.id}\n`);
      return [] as any as (JiraResponse & JiraSprintsResponse);
    }
  }));

  return compact(flatten(boardSprints.map((bs) => bs.values)));
}

export async function getSprints(request: { boardId: number }) {
  return execute<JiraSprintsResponse>(`/rest/agile/1.0/board/${request.boardId}/sprint`);
}

export async function getStatusList() {
  return execute<JiraIssueStatus[]>(`/rest/api/latest/status`);
}

async function execute<T = {}>(path: string): Promise<JiraResponse & T> {

  const { credentials, url } = context;

  const { hostname, port, protocol } = Url.parse(url);

  const requestOptions: http.RequestOptions = {
    protocol,
    host: hostname,
    port,
    path,
    auth: `${credentials.username}:${credentials.password}`
  };
  try {
    const response = await new Promise<http.IncomingMessage>(
      (resolve) => ((requestOptions.protocol === "https:" ? https : http) as typeof http).get(requestOptions, resolve));

    if (response.statusCode && response.statusCode >= 400) {
      return Promise.reject("failed to execute " + path + " " + response.statusCode);
    }

    let json = "";
    response.on("data", (buffer) => json += buffer);
    await new Promise<string>((resolve) => response.on("end", resolve));

    try {
      const jiraResponse = JSON.parse(json.toString()) as JiraResponse & T;
      return jiraResponse;
    } catch (err) {
      console.error("unable to parse json", json.toString());
      return Promise.reject(err);
    }
  } catch (err) {
    console.log("failed to execute ", path, credentials);
    return Promise.reject(err);
  }
}
