import { orderBy, sum, takeWhile } from "lodash";
import { JiraIssue, JiraIssueFields } from "./jira-client";

interface LimitWorkloadOptions {
  orderBy?: keyof JiraIssueFields;
}

const DEFAULT_OPTIONS: LimitWorkloadOptions = {};

/**
 * Limits the issues by the provided workload
 * Eg.g you hav a 2h, a 6h and a 4h issue and limit by 8h
 * Only the 2h and 6h issues will be returned
 */
export function limitWorkload(issues: JiraIssue[], maxWorkloadSeconds: number, options = DEFAULT_OPTIONS) {

  if (options.orderBy !== undefined) {
    // order the issues by an issue field
    issues = orderBy<JiraIssue>(issues, (i: JiraIssue) => i.fields[options.orderBy as keyof JiraIssueFields]);
  }

  let remainingWorkload = maxWorkloadSeconds;

  // return issues until the remaining workload is depleted
  return takeWhile(
    issues,
    (i: JiraIssue) => {
      const canTake = remainingWorkload > 0;
      remainingWorkload -= i.fields.timeestimate;
      return canTake;
    }
  );
}

/**
 * Calculates the sum of an issue field
 */
export function issueSum(issues: JiraIssue[], field: keyof JiraIssueFields) {
  return sum(issues.map(({fields}) => fields[field]));
}
