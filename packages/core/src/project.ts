import * as fs from "fs";
import { memoize } from "lodash";
import * as path from "path";

export interface JiraShellProject {
  url: string;
  username?: string;
  project: string;
  customFields?: Record<string, string>;
}

declare var __TEST__: boolean;

/**
 * searches for a '.jira-shell' file in the cwd and all of its ancestors
 */
export const getJiraShellProject: () => JiraShellProject = memoize(() => {

  // tslint:disable-next-line:no-string-literal
  if ((global as any)["__TEST__"]) {
    return UNDEFINED_PROJECT;
  }

  let currentPath = process.cwd();

  while (!fs.existsSync(path.resolve(currentPath, ".jira-shell"))) {
    const parentPath = path.resolve(currentPath, "..");
    if (parentPath === currentPath) {
      break;
    }
    currentPath = parentPath;
  }

  if (fs.existsSync(path.resolve(currentPath, ".jira-shell"))) {
    const json = fs.readFileSync(path.resolve(currentPath, ".jira-shell"));
    return JSON.parse(json.toString()) as JiraShellProject;
  }

  return UNDEFINED_PROJECT;
});

export const UNDEFINED_PROJECT = { url: undefined as any, project: "<UNDEFINED>" };
