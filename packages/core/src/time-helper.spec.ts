import { getPreviousWorkday, JsDay } from "./time-helper";

describe("time helpers", () => {
  describe("getPreviousWorkday", () => {
    it("should get the prev day from thu-fr with default workdays", () => {
      const testData = [
        // tuesday => monday
        { day: "2018-10-23", prev: "2018-10-22" },
        // wednesday => tuesday
        { day: "2018-10-24", prev: "2018-10-23" },
        // thursday => wednesday
        { day: "2018-10-25", prev: "2018-10-24" },
        // friday => thursday
        { day: "2018-10-23", prev: "2018-10-22" }
      ];
      testData.forEach(({ day, prev }) => {
        const actual = getPreviousWorkday(new Date(day));
        const expected = new Date(prev);
        expect(actual).toEqual(expected);
      });
    });

    it("should get friday as last workday for a monday", () => {
      const actual = getPreviousWorkday(new Date("2018-10-22"));
      const expected = new Date("2018-10-19");
      expect(actual).toEqual(expected);
    });

    it("should get the right day for custom workdays", () => {
      const workdays = [2, 5]; // tuesday and friday
      const testData = [
        // tuesday => friday
        { day: "2018-10-23", prev: "2018-10-19"},
        // friday => tuesday
        { day: "2018-10-26", prev: "2018-10-23"},
        // sunday => friday
        { day: "2018-10-28", prev: "2018-10-26"}
      ];

      testData.forEach(({day, prev}) => {
        const actual = getPreviousWorkday(new Date(day), workdays);
        const expected = new Date(prev);
        expect(actual).toEqual(expected);
      });
    });
  });
});
