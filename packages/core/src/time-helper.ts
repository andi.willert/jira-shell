import * as dateFns from "date-fns";

/**
 * Enum based on the JS getDay() Method
 */
export enum JsDay {
  SUN,
  MON,
  TUE,
  WEN,
  THU,
  FRI,
  SAT
}

const defaultWorkdays = [
  JsDay.MON, JsDay.TUE, JsDay.WEN, JsDay.THU, JsDay.FRI
];

/**
 * Calculates the previous workday based on the "date" parameter and the workdays list
 * @param date: the date you want to get the previous workingday of. default: today
 * @param workdays: list of workdays; default: Mon-Fri
 */
export function getPreviousWorkday(date: Date = new Date(), workdays: JsDay[] = defaultWorkdays): Date {
  const yesterday = dateFns.subDays(date, 1);
  return workdays.includes(yesterday.getDay())
    ? yesterday
    : getPreviousWorkday(yesterday, workdays);
}
