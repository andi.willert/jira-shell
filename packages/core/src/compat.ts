import chalk from "chalk";

/**
 * Chek if the current node.js version is >=10
 * Print a warning otherwise
 */
export function checkEngineCompatibility() {
  if (!/^v[1-9][0-9]+/.test(process.version)) {
    process.stdout.write(chalk.bold.yellow([
      "## WARNING: jira-shell only supports node.js version >=10",
      "## go to: 'https://nodejs.org/' and grab a update\n"
    ].join("\n")));
  }
}
